<?php

//@TODO Recuperar estos datos desde el fichero .env
$DB_CONNECTION = "mysql";
$DB_HOST = "localhost";
$DB_PORT = "3306";
$DB_DATABASE = "rendimiento_limit";
$DB_USERNAME = "debian";
$DB_PASSWORD = "debian";
$Connection = new PDO("mysql:dbname=$DB_DATABASE;host=$DB_HOST", $DB_USERNAME, $DB_PASSWORD);


$Elementos = 1;
for($i=1; $i<5; ++$i){ //8
    $Elementos *= 10;

    cronometradoGenerarElementos($Connection, $Elementos);
    cronometradoGenerarElementosPack($Connection, $Elementos);
}

function cronometradoGenerarElementos($Connection, $Elementos){
    resetTable($Connection);

    $time_start = microtime(true);

    generarElementos($Connection, $Elementos);

    $time_end = microtime(true);
    $time = $time_end - $time_start;
    echo 'Creating '.$Elementos.' elements : '.$time.' ms'."\n";
}

function cronometradoGenerarElementosPack($Connection, $Elementos){
    resetTable($Connection);

    $time_start = microtime(true);

    generarElementosPack($Connection, $Elementos);

    $time_end = microtime(true);
    $time = $time_end - $time_start;
    echo 'Creating PACK '.$Elementos.' elements : '.$time.' ms'."\n";
}

function generarElementos($Connection, $total){
    for($i=1; $i<=$total; ++$i){
        set($Connection, $i);
    }
}

function set($Connection, $i) {
    try {
        $Sql = "INSERT INTO rendimiento_limit SET id2 = " . $i . " ; ";
        $stmt = $Connection->prepare($Sql);
        $stmt->execute();
    } catch (PDOexception $e) {
        print_r($e);
    }
}

function generarElementosPack($Connection, $total){
    $Pack = "";
    for($i=1; $i<=$total; ++$i){
        $Pack .= "(".$i."),";
    }
    $Pack = trim($Pack, ','); //Quitar la última ,

    setPack($Connection, $Pack);
}

function setPack($Connection, $Pack) {
    try {
        $Sql = "INSERT INTO rendimiento_limit (id2) VALUES " . $Pack . " ; ";
        $stmt = $Connection->prepare($Sql);
        $stmt->execute();
    } catch (PDOexception $e) {
        print_r($e);
    }
}

function resetTable($Connection){
    try {
        $Sql = "TRUNCATE TABLE rendimiento_limit ; ";
        $stmt = $Connection->prepare($Sql);
        $stmt->execute();
    } catch (PDOexception $e) {
        print_r($e);
    }    
}
<?php

//@TODO Recuperar estos datos desde el fichero .env
$DB_CONNECTION = "mysql";
$DB_HOST = "localhost";
$DB_PORT = "3306";
$DB_DATABASE = "rendimiento_limit";
$DB_USERNAME = "debian";
$DB_PASSWORD = "debian";
$Connection = new PDO("mysql:dbname=$DB_DATABASE;host=$DB_HOST", $DB_USERNAME, $DB_PASSWORD);


$Elementos = 1;
for($i=1; $i<7; ++$i){ //8
    $Elementos *= 10;

    cronometradoGenerarElementosPack($Connection, $Elementos);
}

function cronometradoGenerarElementosPack($Connection, $Elementos){
    resetTable($Connection);

    $time_start = microtime(true);

    generarElementosPack($Connection, $Elementos);

    $time_end = microtime(true);
    $time = $time_end - $time_start;
    echo 'Creating PACK '.$Elementos.' elements : '.$time.' ms'."\n";

    //Generacion de 50 consultas sobre id
    generar50ConsultasId($Connection, $Elementos);

    //Generacion de 50 consultar sobre id2
    generar50ConsultasId2($Connection, $Elementos);

    //Crear índice para id2
    generarIndiceUnico($Connection);
    //Generacion de 50 consultar sobre id2 idexado
    generar50ConsultasId2Indexado($Connection, $Elementos);
    //Eliminar el indice para id2
    eliminarIndice($Connection);

    //Crear índice para id2
    generarIndiceOrdinario($Connection);
    //Generacion de 50 consultar sobre id2 idexado
    generar50ConsultasId2Indexado($Connection, $Elementos);
    //Eliminar el indice para id2
    eliminarIndice($Connection);    
}

function generarElementosPack($Connection, $total){
    $Pack = "";
    for($i=1; $i<$total; ++$i){
        $Pack .= "(".$i."),";
    }
    $Pack = trim($Pack, ','); //Quitar la última ,

    setPack($Connection, $Pack);
}

function setPack($Connection, $Pack) {
    try {
        $Sql = "INSERT INTO rendimiento_limit (id2) VALUES " . $Pack . " ; ";
        $stmt = $Connection->prepare($Sql);
        $stmt->execute();
    } catch (PDOexception $e) {
        print_r($e);
    }
}

function resetTable($Connection){
    try {
        $Sql = "TRUNCATE TABLE rendimiento_limit ; ";
        $stmt = $Connection->prepare($Sql);
        $stmt->execute();
    } catch (PDOexception $e) {
        print_r($e);
    }    
}

function generar50ConsultasCore($Connection, $Elementos, $SqlBase){
    $TotalSin = 0;
    $TotalCon = 0;
    $NumeroConsultas = 5000;
    for($i=0;$i<$NumeroConsultas;++$i){
        $idBuscado = rand(1,$Elementos);
        $Sql = $SqlBase.$idBuscado;

        $time_start = microtime(true);

        try{
            $stmt = $Connection->prepare($Sql);
            $stmt->execute();
        }catch(Exception $e){
            print_r($e);
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $TotalSin += $time;      
    }

    for($i=0;$i<$NumeroConsultas;++$i){
        $idBuscado = rand(1,$Elementos);
        $Sql = $SqlBase.$idBuscado.= " limit 1";

        $time_start = microtime(true);

        $stmt = $Connection->prepare($Sql);
        $stmt->execute();

        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $TotalCon += $time;
    }
    echo 'ELEMENTOS = '.$Elementos."\n";
    echo 'SIN  '.$TotalSin.' ms'."\n";  
    echo 'CON  '.$TotalCon.' ms'."\n";                
    echo "\n";
}

function generar50ConsultasId($Connection, $Elementos){
        $SqlBase = "SELECT id FROM rendimiento_limit WHERE id = ";
        echo "generar50ConsultasId................."."\n";
        generar50ConsultasCore($Connection, $Elementos, $SqlBase);
}

function generar50ConsultasId2($Connection, $Elementos){
    $SqlBase = "SELECT id2 FROM rendimiento_limit WHERE id2 = ";
    echo "generar50ConsultasId2................."."\n";
    generar50ConsultasCore($Connection, $Elementos, $SqlBase);
}

function generar50ConsultasId2Indexado($Connection, $Elementos){
    $SqlBase = "SELECT id2 FROM rendimiento_limit WHERE id2 = ";
    echo "generar50ConsultasId2.Indexado..........."."\n";
    generar50ConsultasCore($Connection, $Elementos, $SqlBase);
}

function generarIndiceUnico($Connection){
    echo "Indice Único"."\n";
    $Sql = "ALTER TABLE rendimiento_limit ADD UNIQUE ´id2´ (´id2´);";
    $stmt = $Connection->prepare($Sql);
    $stmt->execute();
}

function generarIndiceOrdinario($Connection){
    echo "Indice Ordinario"."\n";
    $Sql = "ALTER TABLE rendimiento_limit ADD ´id2´ (´id2´);";
    $stmt = $Connection->prepare($Sql);
    $stmt->execute();
}

function eliminarIndice($Connection){
    $Sql = "DROP INDEX ´id2´ ON rendimiento_limit;";
    $stmt = $Connection->prepare($Sql);
    $stmt->execute();
}


